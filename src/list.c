#include <stdlib.h>

struct cell_t
{
  void *val;
  unsigned long int id;
  struct cell_t *next;
};

typedef struct cell_t *list_t;

list_t list_empty()
{
  return NULL;
}

int list_is_empty(list_t l)
{
  return l == NULL;
}

list_t list_push(list_t l, void *x)
{
  list_t cell = malloc(sizeof(*cell));
  cell->val = x;
  if (list_is_empty(l))
  {
    cell->id = 1;
    cell->next = NULL;
  }
  else
  {
    cell->id = l->id + 1;
    cell->next = l;
  }
  return cell;
}

list_t list_tail(list_t l)
{
  if (list_is_empty(l))
    return NULL;

  return l->next;
}

void *list_pop(list_t *l)
{
  if (list_is_empty(*l))
    return NULL;
  list_t tmp = *l;
  *l = tmp->next;
  return tmp->val;
}

void *list_top(list_t l)
{
  if (list_is_empty(l))
    return NULL;

  return l->val;
}

void list_destroy(list_t l, void (*free_void)(void *))
{
  while (!list_is_empty(l))
  {
    free(list_pop(&l));
  }
}

// return the found element or NULL
void *list_in(list_t l, void *x, int (*eq)(void *, void *))
{
  while (!list_is_empty(l))
  {
    if (eq(x, l->val))
      return l->val;
    l = l->next;
  }
  return NULL;
}

unsigned long int list_len(list_t l)
{
  unsigned long int len = 0;
  while (!list_is_empty(l))
  {
    len++;
    l = l->next;
  }
  return len;
}
